function shuffle(a) {
    var j, x, i;
    for (i = a.length - 1; i > 0; i--) {
        j = Math.floor(Math.random() * (i + 1));
        x = a[i];
        a[i] = a[j];
        a[j] = x;
    }
    return a;
}

// Create a request variable and assign a new XMLHttpRequest object to it.
var request = new XMLHttpRequest();

// Open a new connection, using the GET request on the URL endpoint
request.open('GET', 'https://opentdb.com/api.php?amount=10&type=multiple', true);

request.onload = function () {
  // Begin accessing JSON data here
  var data = JSON.parse(this.response);

  questionNumber = 0;

  if (request.status >= 200 && request.status < 400) {
    data.results.forEach(item => {
      questionNumber++;
      // Log each movie's title
      const app = document.getElementById('quiz');
      app.setAttribute('class', 'container');

      const quest = document.createElement('p');
      quest.setAttribute('class', 'title');
      quest.textContent = item.question;

      const category = document.createElement('p');
      category.setAttribute('class', 'category');
      category.textContent = item.category;

      const incorAnswers = item.incorrect_answers;
      const corAnswer = item.correct_answer;
      const answers = incorAnswers;
      answers.push(corAnswer);

      shuffle(answers);

      // app.appendChild(container);
      app.appendChild(quest);
      app.appendChild(category);

      console.log(questionNumber);

      answerNumber=0;
      i=0;
      answers.forEach(answer => {
        answerNumber++;
        const input = document.createElement('input');
        const label = document.createElement('label');
        const br = document.createElement('br');
        label.textContent = answers[i];
        input.setAttribute('type', 'radio');
        app.appendChild(input);
        app.appendChild(label);
        app.appendChild(br);
        i++;
      });



    });
  } else {
    console.log('error');
  }

  function showResults(){
    const answerContainers = answers.querySelectorAll('.answers');

  };

}

// Send request
request.send();
